def calculo_tss_rd(total_ingresos=0,residente_legal='S'):
	total_ingresos_tss = total_ingresos
	salario_minimo_cotizable = 9855.00
	afp_porciento = 2.87
	sfs_porciento = 3.04
	tope_afp = salario_minimo_cotizable * 20
	tope_sfs = salario_minimo_cotizable * 10
	
	
	# Se verifica el monto de total_ingresos_tss con los topes de las deducciones impositivas
	if total_ingresos_tss > tope_afp:
		total_ingresos_tss_afp = tope_afp
	else:
		total_ingresos_tss_afp = total_ingresos_tss
	
	
	if total_ingresos_tss > tope_sfs:
		total_ingresos_tss_sfs = tope_sfs
	else:
		total_ingresos_tss_sfs = total_ingresos_tss

		
	afp_calculo = (total_ingresos_tss_afp * afp_porciento)/100
	sfs_calculo = (total_ingresos_tss_sfs * sfs_porciento)/100
	
	
	monto_tss = afp_calculo + sfs_calculo
	
	
	if residente_legal == 'N':
		monto_tss = 0
	
	return monto_tss
	


def calculo_isr_rd(total_ingresos=0,total_ingresos_tss=0,descuento_percapita_tss=0,residente_legal='S',empleado_directo='N'):
	# Se definen las escalas vigentes a Enero 2016, consultadas desde la DGII, 
	# http://www.dgii.gov.do/informacionTributaria/principalesImpuestos/Paginas/Impuesto-Sobre-la-Renta.aspx
	escala_1_desde = 0
	escala_1_hasta = 409281.00
	escala_2_desde = escala_1_hasta + 0.01
	escala_2_hasta = 613921.00
	escala_3_desde = escala_2_hasta + 0.01
	escala_3_hasta = 852667.00
	escala_3_monto = 30696.00
	escala_4_desde = escala_3_hasta + 0.01 
	escala_4_hasta = 999999999999999 
	escala_4_monto = 78446.00
	
	
	# Se Calcula el monto de SFS y AFP
	calculo_tss = calculo_tss_rd (total_ingresos,residente_legal)
	# Se Calcula el Imponible
	total_ingresos_imponible = total_ingresos - calculo_tss - descuento_percapita_tss
	# Se Proyecta el imponible a 12 meses
	total_ingresos_imponible = total_ingresos_imponible * 12	
	
	
	if escala_1_desde <= total_ingresos_imponible <= escala_1_hasta:
		descuento_isr_total = 0
		descuento_isr_total_mensual = 0
	elif escala_2_desde <= total_ingresos_imponible <= escala_2_hasta:
		excedente = total_ingresos_imponible - escala_2_desde
		descuento_isr_total = excedente * 0.15
		descuento_isr_total_mensual = descuento_isr_total / 12
	elif escala_3_desde <= total_ingresos_imponible <= escala_2_hasta:
		excedente = total_ingresos_imponible - escala_3_desde
		descuento_isr_total = (excedente * 0.20) + escala_3_monto
		descuento_isr_total_mensual = descuento_isr_total / 12
	elif escala_4_desde <= total_ingresos_imponible <= escala_4_hasta:
		excedente = total_ingresos_imponible - escala_4_desde
		descuento_isr_total = (excedente * 0.25) + escala_4_monto
		descuento_isr_total_mensual = descuento_isr_total / 12
	
	if empleado_directo=='S':
		descuento_isr_total_mensual =  (total_ingresos* 0.10)
		
		
	return "%0.2f" % descuento_isr_total_mensual
	
monto_calculo = int(input("Introduce el monto total percibido por el empleado:\n"))	
proceso = calculo_isr_rd(monto_calculo)
print('-:==================CALCULO ISR REP DOM==================:-')
print('Monto ISR ',calculo_isr_rd(monto_calculo))
print('-:==================CALCULO ISR REP DOM==================:-')
